const byte ledPin = 16;
const byte buttonPin = D6;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  digitalWrite(ledPin, HIGH);

  while (digitalRead(buttonPin))
  {
    yield();
  }

}

void loop() {
  digitalWrite(ledPin, LOW);

}
