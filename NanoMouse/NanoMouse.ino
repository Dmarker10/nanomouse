#include <Servo.h>

#define RIGHT -1 // oppsite of the video 
#define LEFT 1
Servo leftServo;
Servo rightServo;

const byte ledPin = 16;
const byte buttonPin = D6;

const byte power = 500;

void forward()
{
  leftServo.writeMicroseconds(1440+power);
  rightServo.writeMicroseconds(1530-power);
}

void forwardTime(unsigned int time)
{
  forward();
  delay(time);
  stop();
}


void stop()
{
  leftServo.writeMicroseconds(1500);
  rightServo.writeMicroseconds(1500);
 
}

void turn(int direction, int degrees)
{
  leftServo.writeMicroseconds(1500+power*direction);
  rightServo.writeMicroseconds(1500+power*direction);
  delay(degrees*6);
  stop();
}

void setup() {
  leftServo.attach(D4);
  rightServo.attach(10);
 
  pinMode(ledPin,OUTPUT);
  pinMode(buttonPin,INPUT_PULLUP);

  while(digitalRead(buttonPin))
  {
    }

    turn(LEFT,180);


 
}

void loop() {
  digitalWrite(ledPin,HIGH);
 

}